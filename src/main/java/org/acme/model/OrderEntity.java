/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.acme.model;

import io.quarkus.hibernate.orm.panache.Panache;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author dbrage
 */
@Entity
@Getter
@Setter
public class OrderEntity extends Panache{

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    public Long id;

    private OrderType type;

    private double cost;

    private enum OrderType{
        DRAFT,
        FOR_APPROVAL,
        APPROVED
    }
}
