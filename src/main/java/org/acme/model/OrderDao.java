package org.acme.model;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import javax.ws.rs.PathParam;
import java.util.List;

@ApplicationScoped
public class OrderDao {

    @PersistenceContext
    EntityManager em;

    public List<OrderEntity> findAll() {
        return em.createQuery("from OrderEntity").getResultList();
    }

    public OrderEntity findById(String id) {
        return em.find(OrderEntity.class, id);
    }

    @Transactional
    public boolean persist(OrderEntity data) {
        em.persist(data);
        return true;
    }
}
