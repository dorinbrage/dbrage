package org.acme.model;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import javax.ws.rs.PathParam;
import java.util.List;

@ApplicationScoped
public class CustomerDao {

    @PersistenceContext
    EntityManager em;

    public List<CustomerEntity> findAll() {
        return em.createQuery("from CustomerEntity").getResultList();
    }

    public CustomerEntity findById(String id) {
        return em.find(CustomerEntity.class, id);
    }

    @Transactional
    public boolean persist(CustomerEntity data) {
        em.persist(data);
        return true;
    }
}
