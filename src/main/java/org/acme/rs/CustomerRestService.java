package org.acme.rs;

import org.acme.model.CustomerDao;
import org.acme.model.CustomerEntity;
import org.acme.model.OrderDao;
import org.acme.model.OrderEntity;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/customers")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class CustomerRestService {

    @Inject
    private CustomerDao dao;

    @GET
    public List<CustomerEntity> findAll() {
        return dao.findAll();
    }

    @POST
    public Response persist(CustomerEntity data){
        boolean result = dao.persist(data);
        Response response = result ? Response.status(202).build() : Response.status(406).build();
        return response;
    }
}
