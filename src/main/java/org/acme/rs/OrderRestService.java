package org.acme.rs;

import org.acme.model.OrderDao;
import org.acme.model.OrderEntity;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/orders")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class OrderRestService {

    @Inject
    private OrderDao dao;

    @GET
    public List<OrderEntity> findAll() {
        return dao.findAll();
    }

    @POST
    public Response persist(OrderEntity data){
        boolean result = dao.persist(data);
        Response response = result ? Response.status(202).build() : Response.status(406).build();
        return response;
    }
}
